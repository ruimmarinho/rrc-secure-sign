from signdemand.models import SignDemand
from django.http import HttpResponse
from rest_framework.views import APIView
from django.conf import settings as conf_settings
import jwt
from signdemand.view.formats import FormatDemand
import os



class originalfileView(APIView):
  
    def get(self, request):

        try:
        
            token2=request.GET.get('auth')
            token2=jwt.decode(token2,conf_settings.JWT_TOKEN,algorithms="HS256")
            userDemand = SignDemand.objects.get(id=token2['id'])
            userDemand=FormatDemand().format(userDemand)
            fileDemand=userDemand['original_file']
 
            dir = "original_file"
            fileBytes = []
            with os.scandir(dir) as ficheros:
                for fichero in ficheros:
                    if ("/"+fichero.name==fileDemand):
                        with open(fichero.path,"rb") as f:
                            fileBytes = f.read()
                            break
            if(userDemand):
                return HttpResponse(fileBytes, status=200, content_type="application/pdf")
            else:
                return HttpResponse([], status=400, content_type="application/json")

        except jwt.DecodeError as e:    
            print(e)
            return HttpResponse(e, status=401)

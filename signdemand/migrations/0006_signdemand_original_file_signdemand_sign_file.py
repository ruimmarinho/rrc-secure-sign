# Generated by Django 4.0.3 on 2022-03-23 10:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('signdemand', '0005_alter_signdemand_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='signdemand',
            name='original_file',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='signdemand',
            name='sign_file',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
    ]

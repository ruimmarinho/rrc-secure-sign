from urllib import response
from signdemand.models import SignDemand
import json
from django.http import HttpResponse
from rest_framework.views import APIView
from django.conf import settings as conf_settings
import jwt
from signdemand.view.formats import FormatDemand

class demandbytokenView(APIView):
  
    def get(self, request):

        try:
        
            token2=request.GET.get('auth')
            token2=jwt.decode(token2,conf_settings.JWT_TOKEN,algorithms="HS256")

            userDemand = SignDemand.objects.get(id=token2['id'])
            userDemand=FormatDemand().format(userDemand)

            if(userDemand):
                return HttpResponse(json.dumps(userDemand), status=200, content_type="application/json")

            else:
                
                return HttpResponse([], status=400, content_type="application/json")

        except jwt.DecodeError as e:    
            # Return a 401 unauthorized error
            print(e)
            return HttpResponse(e, status=401)







from usermgmt.models import AuthorizationToken

class TokensView:
    def isAuth(request):
        token = request.META['HTTP_AUTHORIZATION']
        if AuthorizationToken.objects.filter(token=token).exists():
            return True
        else:
            return False

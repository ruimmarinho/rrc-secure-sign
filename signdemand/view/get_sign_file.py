from msilib.schema import File
from django.http import HttpResponse
from rest_framework.views import APIView
from django.conf import settings as conf_settings
import jwt
from signdemand.view.formats import FormatDemand
from signdemand.models import SignDemand
import os
from usermgmt.models import AuthorizationToken



class signfileView(APIView):
  def get(self, request, id=0):

        try:
            token = request.META['HTTP_AUTHORIZATION']
            print(token)
            jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])
            AuthorizationToken.objects.get(token=token)
        

            sign_file_demand = SignDemand.objects.get(id=id)
            sign_file_demand=FormatSignFile().format(sign_file_demand)
            fileDemand=sign_file_demand['sign_file']
 
            dir = "sign_file"
            fileBytes = []
            with os.scandir(dir) as ficheros:
                for fichero in ficheros:
                    if ("/"+fichero.name==fileDemand):
                        with open(fichero.path,"rb") as f:
                            fileBytes = f.read()
                            break
            

            if(sign_file_demand):
                return HttpResponse(fileBytes, status=200, content_type="application/pdf")
            else:
                return HttpResponse([], status=400, content_type="application/json")

        except jwt.DecodeError as e:    
            # Return a 401 unauthorized error
            print(e)
            return HttpResponse(e, status=401)


class FormatSignFile():

    @staticmethod
    def format(demand):
        jsonObject = {
                      'sign_file': demand.sign_file.url if demand.sign_file else "url or path to some your default image"
                      }

        return jsonObject

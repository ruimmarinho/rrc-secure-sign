
from django.urls import path, re_path
from django.views import View

from signdemand.view.get_all_demands import alldemandsView
from signdemand.view.get_original_file import originalfileView
from signdemand.view.get_sign_file import signfileView
from signdemand.view.post_sign_demand import signdemandView
from signdemand.view.get_demand_token import demandbytokenView
from signdemand.view.post_reject_sign_demand import rejectsigndemandView
from signdemand.view.post_sign_demand_acept import  aceptsigndemandView




urlpatterns = [
 
    path('demands', alldemandsView.as_view(), name='demands_list'),
    #re_path('^prueba/(?P<title>.+)/$', searchdemandsView.as_view(), name='search'), 
    path('original_file', originalfileView.as_view(), name='original_file'),
    path('sign_file/<int:id>', signfileView.as_view(), name='sign_file'),
    path('demandtoken', demandbytokenView.as_view(), name='demand_create'),
    path('demand', signdemandView.as_view(), name='demand_create'),
    path('sign', aceptsigndemandView.as_view(), name='sign_document'),
    path('reject', rejectsigndemandView.as_view(), name='reject_demand'),


]


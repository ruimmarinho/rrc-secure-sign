import profile
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.conf import settings as conf_settings
from datetime import datetime, timedelta
from usermgmt.tokens_view import TokensView

from .models import Profile
from .models import AuthorizationToken

import json
import jwt


def index(request):
    return HttpResponse("Welcome to UserManagement")


# This class allows users to log-in into the system
class LogInView(APIView):
    # POST Method, that receives the username and password of the user who wants to log-in
    def post(self, request, pk=None):

        # Obtain the email and password for the body of the request, which is a JSON
        json_body = json.loads(request.body)
        email = json_body['email']
        password = json_body['password']

        # Verify username and password are correct
        try:
            user = Profile.objects.get(email=email, password=password, is_active=1)
        except Profile.DoesNotExist:
            user = None

        # Check if authentication is right for the given data
        if user is not None:

            # Create a token for the authenticated user. If the token already exists, get it
            token = jwt.encode({'profile_id': user.id, 'create_date': str(datetime.today())}, conf_settings.JWT_KEY)

            token = token.decode('utf-8')
            add_token_db = AuthorizationToken(token=token, profile=user)
            add_token_db.save()

            # Create a response with the token key and the user id
            return HttpResponse(json.dumps({'authorization_token': token, 'user_id': user.pk}),
                                status=200, content_type="application/json")

        else:
            # Return a 401 unauthorized error
            return HttpResponse('Unauthorized', status=401)


class validation(APIView):
    def get(self, request):
        try:
            token = request.META['HTTP_AUTHORIZATION']
            token = jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])
            #userDemand = Profile.objects.get(profile_id=token['profile_id'])
            print( token['profile_id'])
            profile_validation = Profile.objects.get(id=token['profile_id'])
            profile_validation = FormatProfile().format(profile_validation)
            token=token['profile_id']
            profile_validation=profile_validation['user_id']
            if (token==profile_validation):
                return HttpResponse(True,
                                    status=200, content_type="application/json")

            else:
                # Return a 401 unauthorized error
                return HttpResponse(False, status=401)  
        except jwt.DecodeError as e:    
            # Return a 401 unauthorized error
            print(e)
            return HttpResponse(False, status=401)


class LogOutView(APIView):
    def get(self, request):
        # Check authentication token
        if TokensView.isAuth(request):
            try:
                # Obtain the related token
                token = request.META['HTTP_AUTHORIZATION']

                # Check if token exists and delete it
                token_user = AuthorizationToken.objects.get(token=token)
                token_user.delete()

                return HttpResponse('Logout', status=200)

            except Exception as e:
                return HttpResponse('Token not found', status=404)

        else:
            return HttpResponse('Unauthorized', status=401)

    def delete(self, request):
        # Check authentication token
        if TokensView.isAuth(request):
            try:
                # Obtain the related token
                token = request.META['HTTP_AUTHORIZATION']
                # Obtain the id from user
                profile_id = jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])['profile_id']
                # Delete all tokens with the user id
                AuthorizationToken.objects.filter(profile=profile_id).delete()
                return HttpResponse('Logout all devices', status=200)

            except Exception as e:
                return HttpResponse('Token not found', status=404)

        else:
            return HttpResponse('Unauthorized', status=401)


# This class is used to refresh an authorization token
class RefreshTokenView(APIView):
    # GET Method, which receives the current token in order to generate a new one
    def get(self, request):
        # Check authentication token
        if TokensView.isAuth(request):
            # Obtain the related token with is created date and id from user
            token = request.META['HTTP_AUTHORIZATION']
            profile_id = jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])['profile_id']
            created_date = datetime.strptime(
                jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])['create_date'],
                '%Y-%m-%d %H:%M:%S.%f')

            # Get the current date
            current_date = datetime.now()

            # Check if token has created more than 15 days
            if current_date >= (created_date + timedelta(days=15)):
                # Yes, delete token from database
                token_db = AuthorizationToken.objects.get(token=token)
                token_db.delete()
                return HttpResponse('Token deleted', status=200)

            else:
                # No, generate new token with new created date and update the current token
                new_token = jwt.encode({'profile_id': profile_id, 'create_date': str(datetime.today())},
                                       conf_settings.JWT_KEY,
                                       algorithm='HS256')

                add_token_db = AuthorizationToken.objects.get(token=token)

                add_token_db.token = new_token.decode('utf-8')
                add_token_db.save()
                return Response({
                    'authorization_token': new_token
                })

        else:
            return HttpResponse('Unauthorized', status=401)


# Class to obtain user data based on user id
class RegisterUser(APIView):
    def post(self, request, pk=None):
        # Obtain the username and password for the body of the request, which is a JSON
        json_body = json.loads(request.body)
        name = json_body['name']
        surname = json_body['surname']
        title = json_body['title']
        phone = json_body['phone']
        email = json_body['email']
        password = json_body['password']

        # Verify if email already used
        try:
            user = Profile.objects.get(email=email)
        except Profile.DoesNotExist:
            user = None

        # Check if authentication is right for the given data
        if user is None:

            # Create an inactive user
            add_user_db = Profile(name=name, surname=surname, title=title, phone=phone, email=email, password=password)
            add_user_db.save()

            # Create a response with the token key and the user id
            return HttpResponse('User Registered', status=200,
                                content_type="application/json")

        else:
            # Return a 401 unauthorized error
            return HttpResponse('User Already Registered', status=401)


# Class to obtain user data based on user id
class GetUser(APIView):
    def get(self, request, user_id=-1, pk=None):
        # Check authentication token
        # if TokensView.isAuth(request):
            # Obtain the user data, checking if the user is the own user (the authenticated) or
            # otherwise, the user is another user which data is taken from the database
            try:
                # Calculate the user id of the profile, and get the profile from database
                token = request.META['HTTP_AUTHORIZATION']
                profile_id = jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])['profile_id']
                profile_user_id = profile_id if user_id == -1 or user_id == profile_id else user_id
                profile = Profile.objects.get(pk=profile_user_id)


                # Create a response with the user data
                jsonProfile = FormatProfile().format(profile)
                return HttpResponse(json.dumps(jsonProfile), status=200, content_type="application/json")

            except Profile.DoesNotExist:
                # Return a 404 unauthorized error
                return HttpResponse('User not found', status=404)

        # else:
            # return HttpResponse('Unauthorized', status=401)


class FormatProfile():

    # Status:
    #   0 - Green
    #   1 - Yellow
    #   2 - Red

    # This method formats the data of a profile into a JSON Object
    @staticmethod
    def format(profile):

        # Iterate WeeklyTasks to count total vs completed

        jsonObject = {'user_id': profile.pk,
                      'name': profile.name,
                      'surname': profile.surname,
                      'email': profile.email,
                      'is_active': profile.is_active,
                      'title': profile.title,
                      'image_url': profile.image_url,
                      }

        return jsonObject

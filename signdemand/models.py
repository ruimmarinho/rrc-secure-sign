from datetime import datetime
from email.policy import default
from operator import truediv
from django.db import models

#from rrcsecuresign.settings import MEDIA_ROOT

# Create your models here.

class SignDemand(models.Model):
    title=models.CharField(max_length=255)
    emision_date=models.DateTimeField(default=datetime.now)
    status=models.IntegerField(default=0)
    original_file=models.FileField(blank=True, null=True)
    #email=models.EmailField()
    #password = models.CharField(max_length=15)
    sign_file=models.FileField( blank=True, null=True)
    signer_name=models.CharField(max_length=255)
    signer_lastname=models.CharField(max_length=255)
    signer_email=models.EmailField()
    sign_date=models.DateTimeField(default=datetime.now)
    rejection_reason=models.TextField(max_length=255)
    sign_token = models.TextField(null=True)

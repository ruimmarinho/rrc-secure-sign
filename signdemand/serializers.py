from rest_framework import serializers
from signdemand.models import SignDemand
 
class SignDemandSerializer(serializers.ModelSerializer):
    class Meta:
        model = SignDemand
        fields = ['id', 'title', 'emision_date', 'status', 'email', 'password', 'signer_name', 'signer_lastname', 'signer_email', 'sign_date', 'rejection_reason']

 


class FormatDemand():

    # Status:
    #   0 - Verde
    #   1 - Amarillo
    #   2 - Rojo

    # This method formats the data of a task into a JSON Object
    @staticmethod
    def format(result):
        jsonObject = {'id': result.id,
                      'title': result.title,
                      'emision_date': str(result.emision_date.strftime('%x')),
                      'sign_date':str(result.sign_date.strftime('%x')),
                      'signer_name': result.signer_name,
                      'signer_lastname': result.signer_lastname,
                      'signer_email': result.signer_email,
                      'rejection_reason': result.rejection_reason,
                      'status': result.status,
                      'original_file': result.original_file.url if result.original_file else "url or path to some your default image",
                      'sign_file': result.sign_file.url if result.sign_file else "url or path to some your default image"
                      }

        return jsonObject

    @staticmethod
    def formatFilter(result):
        jsonObject = {'id': result['id'],
                      'title': result['title'],
                      'emision_date': str(result['emision_date'].strftime('%c')),
                      'sign_date':str(result['sign_date'].strftime('%c')),
                      'signer_name': result['signer_name'],
                      'signer_lastname': result['signer_lastname'],
                      'signer_email': result['signer_email'],
                      'rejection_reason': result['rejection_reason'],
                      'status': result['status'],
                      'original_file': result['original_file'] if result['original_file'] else "url or path to some your default image",
                      'sign_file': result['sign_file'] if result['sign_file'] else "url or path to some your default image"
                      }

        return jsonObject
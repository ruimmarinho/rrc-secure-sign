from django.urls import path
from . import views

urlpatterns = [

    path('', views.index, name='index'),
    path('login/', views.LogInView.as_view()),
    path('validation/', views.validation.as_view()),
    path('logout/', views.LogOutView.as_view()),
    path('refresh-token/', views.RefreshTokenView.as_view()),
    path('register-user/', views.RegisterUser.as_view()),
    path('user/', views.GetUser.as_view()),
    path('user/<int:user_id>/', views.GetUser.as_view()),
]
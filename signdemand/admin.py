from django.contrib import admin

# Register your models here.
from signdemand.models import SignDemand

admin.site.register(SignDemand)

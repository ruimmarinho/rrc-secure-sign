from django.db import models

class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45)
    surname = models.CharField(max_length=45)
    title = models.CharField(max_length=45)
    email = models.CharField(max_length=45)
    password = models.TextField()
    phone = models.CharField(max_length=45)
    last_connection = models.DateTimeField()
    is_active = models.SmallIntegerField(default=0)
    status = models.SmallIntegerField(default=1)
    image_url = models.CharField(max_length=255, default="http://212.227.149.143:8000/static/profile_icon.png")

class AuthorizationToken(models.Model):
    token = models.TextField(null=True)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
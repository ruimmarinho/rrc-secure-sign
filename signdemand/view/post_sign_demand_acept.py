import jwt
from signdemand.models import SignDemand
import json
from django.conf import settings as conf_settings
from django.http import HttpResponse, JsonResponse
from rest_framework.views import APIView
from signdemand.view.formats import FormatDemand
from signdemand.view.formats import FormatDemand
import base64
import os
from fitz import fitz, Rect
import shutil
from django.core.mail.message import EmailMessage



class aceptsigndemandView(APIView):
    def post(self, request):
        try:
            
            jd=json.loads(request.body)
            signDemand = SignDemand.objects.get(id=jd['id'])
            base64_img=jd['sign_file']
            base64_img_bytes = base64_img.encode('utf-8')
            with open('decoded_image.png', 'wb') as file_to_save:
             decoded_image_data = base64.decodebytes(base64_img_bytes)
             file_to_save.write(decoded_image_data)
            fileDemand=signDemand.original_file

            dir = "original_file"
            with os.scandir(dir) as ficheros:
                for fichero in ficheros:
                    if (fichero.name==fileDemand):
                        doc = os.path.join(fichero)
                        doc = fitz.open(fichero.path)
                        def add_footer(pdf):
                            img = decoded_image_data
                            rect = Rect(0, 100, 200, 1330)
                            for i in range(0, pdf.pageCount):
                                page = pdf[i]
                                if not page.is_wrapped:
                                    page.wrap_contents()
                                page.insert_image(rect, stream=img)

                        add_footer(doc)
                      
                        doc.save(str(signDemand.original_file))
                        shutil.move(str(signDemand.original_file),"sign_file")

            signDemand.sign_file=str(signDemand.original_file)
            signDemand.save()         
            signDemand=FormatDemand().format(signDemand)
       

            email = EmailMessage()
            email.subject = "Email desde secure sign, documento firmado"
            email.body = "El usuario "+signDemand['signer_name']+signDemand['signer_lastname']+" con el correo "+signDemand['signer_email']+" ha firmardo el documento "+ signDemand['title']
            email.from_email = "<"+signDemand['signer_email']+">"
            email.to = [signDemand['signer_email'], ] 
            email.attach_file('sign_file/'+signDemand['sign_file'])
            email.send() 

            return HttpResponse('Sign document succesfull', status=204)
        
        except jwt.DecodeError as e:    
            print(e)
            return HttpResponse(e, status=401)
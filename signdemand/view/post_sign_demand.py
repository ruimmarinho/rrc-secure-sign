import shutil
from tokenize import Token
from urllib import request
from django.conf import settings
from django.shortcuts import render
import jwt
from signdemand.models import SignDemand
import json
from django.conf import settings as conf_settings
from django.http import HttpResponse, JsonResponse
from rest_framework.views import APIView
from signdemand.view.formats import FormatDemand
from usermgmt.models import AuthorizationToken
from django.core.mail import send_mail
import base64
from datetime import datetime
import shutil


class signdemandView(APIView):
    def post(self, request):
        try:
            token = request.META['HTTP_AUTHORIZATION']
            print(token)
            jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])
            AuthorizationToken.objects.get(token=token)
            jd=json.loads(request.body)
            base64_file=jd['original_file']
            now = datetime.now()
            timestamp = datetime.timestamp(now)        
            base64_file_bytes = base64_file.encode('utf-8')
            with open(str(timestamp)+'.pdf', 'wb') as file_to_save:
            
            
             decoded_image_data = base64.decodebytes(base64_file_bytes)

             decoded_image_data=file_to_save.write(decoded_image_data)

            shutil.move(str(timestamp)+".pdf","original_file")

            signDemand=SignDemand(original_file=str(timestamp)+".pdf",
                                title=jd['title'],
                                signer_name=jd['signer_name'],
                                signer_lastname=jd['signer_lastname'],
                                signer_email=jd['signer_email'])
            signDemand.save()

            encoded = jwt.encode({'id':signDemand.id}, settings.JWT_TOKEN, algorithm="HS256")
            encoded=encoded.decode('UTF-8')

            email_from=jd['signer_email']
            subject="Email desde secure sign"
            message="Te enviamos desde donde acceder a firmar tu documento"+' http://localhost:4200/sign?auth='+encoded
            for_email=jd['signer_email']
          
            send_mail(subject, message, email_from, #settings.EMAIL_HOST_USER
                [for_email], fail_silently=False)

            return HttpResponse('Demand created successfully', status=204)
        
        except jwt.DecodeError as e:    
            print(e)
            return HttpResponse(e, status=401)
           

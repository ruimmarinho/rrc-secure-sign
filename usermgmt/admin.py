from django.contrib import admin
from .models import Profile, AuthorizationToken

admin.site.register(Profile)
admin.site.register(AuthorizationToken)
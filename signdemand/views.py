import json
from turtle import title
from typing import Generic
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views import View
from rest_framework.views import APIView
from signdemand.models import SignDemand

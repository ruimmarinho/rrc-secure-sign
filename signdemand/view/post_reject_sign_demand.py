from urllib import request
from django.shortcuts import render
import jwt
from signdemand.models import SignDemand
import json
from django.http import HttpResponse
from rest_framework.views import APIView
from signdemand.view.formats import FormatDemand
from django.core.mail import send_mail
from signdemand.view.formats import FormatDemand


class rejectsigndemandView(APIView):
    def post(self, request):
        try:
            jd=json.loads(request.body)
            signDemand = SignDemand.objects.get(id=jd['id'])
            signDemand.rejection_reason=jd['rejection_reason']

            signDemand=FormatDemand().format(signDemand)
            
            email_from= signDemand['signer_email']
            subject="Email desde secure sign, documento no firmado"
            message="El usuario "+signDemand['signer_name']+" "+signDemand['signer_lastname']+" con el correo "+signDemand['signer_email']+" ha decidido no firmar el documento por las siguientes razones: <br>"+ signDemand['rejection_reason']
            for_email=signDemand['signer_email']
            
            send_mail(subject, message, email_from, [for_email])

            return HttpResponse('Rejection reason successfully', status=204)
        
        except jwt.DecodeError as e:    
            print(e)
            return HttpResponse(e, status=401)

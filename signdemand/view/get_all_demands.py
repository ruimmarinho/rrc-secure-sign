from datetime import datetime
from email import message
from webbrowser import get
import jwt
from signdemand.models import SignDemand
import json
from django.conf import settings as conf_settings
from django.http import HttpResponse, JsonResponse, QueryDict
from rest_framework.views import APIView
from rest_framework import generics
from signdemand.view.formats import FormatDemand
from usermgmt.models import AuthorizationToken
from django.db.models import Q
from django.core.paginator import Paginator

class alldemandsView(generics.GenericAPIView):
    def get(self, request):
        try:
            token = request.META['HTTP_AUTHORIZATION']
            jwt.decode(token, conf_settings.JWT_KEY, algorithms=['HS256'])
            AuthorizationToken.objects.get(token=token)         

            all_demands = SignDemand.objects.none()
            
            arguments = {}
            if self.request.query_params.get('title'):
              arguments['title'] = self.request.query_params.get('title')
            if self.request.query_params.get('signer_name'):
               arguments['signer_name'] = self.request.query_params.get('signer_name')
            if self.request.query_params.get('signer_lastname'):
               arguments['signer_lastname'] = self.request.query_params.get('signer_lastname')
            if self.request.query_params.get('signer_email'):
               arguments['signer_email'] = self.request.query_params.get('signer_email')
            if self.request.query_params.get('sign_date'):
               #sign_date_db = datetime.strptime(self.request.query_params.get('sign_date'), '%d/%m/%y %H:%M:%S')
               #arguments['sign_date'] = sign_date_db.strftime("%Y/%m/%d, %H:%M:%S")
                arguments['sign_date__contains'] = self.request.query_params.get('sign_date')
            if self.request.query_params.get('emision_date'):
               arguments['emision_date__contains'] = self.request.query_params.get('emision_date')
            

            if len(arguments) > 0:
                 all_demands = list(SignDemand.objects.filter(**arguments))
            else:
                    all_demands = SignDemand.objects.all()


            paginator = Paginator(all_demands, 20) 
            page_number = request.GET.get('page')
            all_demands = paginator.get_page(page_number)
            pages=paginator.num_pages
            print(pages)
            response = []
            for i in all_demands:
                response.append(FormatDemand().format(i))

            result ={
               "demands":response,
               "pages":pages
            }
            return  HttpResponse((json.dumps(result)), status=200, content_type="application/json" )
        
        except jwt.DecodeError as e:    
            print(e)
            return HttpResponse(e, status=401)